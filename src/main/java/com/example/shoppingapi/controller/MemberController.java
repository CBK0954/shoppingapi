package com.example.shoppingapi.controller;

import com.example.shoppingapi.model.LoginRequest;
import com.example.shoppingapi.model.LoginResponse;
import com.example.shoppingapi.model.SingleResult;
import com.example.shoppingapi.service.MemberService;
import com.example.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "로그인")
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberService.doLogin(loginRequest));
    }
}