package com.example.shoppingapi.controller;

import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.service.BannerService;
import com.example.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "배너 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/banner")
public class BannerController {
    private final BannerService bannerService;

    @ApiOperation(value = "사용중인 배너 리스트")
    @GetMapping("/all/use")
    public ListResult<String> getUseBanners() {
        return ResponseService.getListResult(bannerService.getUseBanners(), true);
    }
}
