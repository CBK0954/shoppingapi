package com.example.shoppingapi.controller;

import com.example.shoppingapi.entity.Member;
import com.example.shoppingapi.model.CommonResult;
import com.example.shoppingapi.model.OrderRequest;
import com.example.shoppingapi.service.MemberService;
import com.example.shoppingapi.service.OrderService;
import com.example.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "주문 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/order")
public class OrderController {
    private final OrderService orderService;
    private final MemberService memberService;

    @ApiOperation(value = "주문 등록")
    @PostMapping("/goods/member-id/{memberId}")
    public CommonResult setOrder(@PathVariable long memberId, @RequestBody @Valid OrderRequest orderRequest) {
        Member member = memberService.getData(memberId);
        orderService.setOrder(member, orderRequest);

        return ResponseService.getSuccessResult();
    }
}