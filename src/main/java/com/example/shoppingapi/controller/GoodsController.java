package com.example.shoppingapi.controller;

import com.example.shoppingapi.enums.GoodsCategory;
import com.example.shoppingapi.model.GoodsItem;
import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.service.GoodsService;
import com.example.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "상품 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @ApiOperation(value = "카테고리별 상품 리스트")
    @GetMapping("/all/category")
    public ListResult<GoodsItem> getGoods(@RequestParam GoodsCategory goodsCategory) {
        return ResponseService.getListResult(goodsService.getGoodsByCategory(goodsCategory), true);
    }
}
