package com.example.shoppingapi.controller;

import com.example.shoppingapi.entity.Goods;
import com.example.shoppingapi.model.CartCreateRequest;
import com.example.shoppingapi.model.CartItem;
import com.example.shoppingapi.model.CommonResult;
import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.service.CartService;
import com.example.shoppingapi.service.GoodsService;
import com.example.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "장바구니")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/cart")
public class CartController {
    private final CartService cartService;
    private final GoodsService goodsService;

    @ApiOperation(value = "장바구니 상품 담기")
    @PostMapping("/goods")
    public CommonResult setCart(@RequestBody @Valid CartCreateRequest cartCreateRequest) {
        Goods goods = goodsService.getData(cartCreateRequest.getGoodsId());
        cartService.setCart(cartCreateRequest.getMemberId(), goods);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "나의 장바구니 목록")
    @GetMapping("/list/my/member-id/{memberId}")
    public ListResult<CartItem> getCartsByMy(@PathVariable long memberId) {
        return ResponseService.getListResult(cartService.getCartsByMy(memberId), true);
    }

    @ApiOperation(value = "장바구니 상품 삭제")
    @DeleteMapping("/cart-id/{cartId}")
    public CommonResult delCart(@PathVariable long cartId) {
        cartService.delCart(cartId);
        return ResponseService.getSuccessResult();
    }
}