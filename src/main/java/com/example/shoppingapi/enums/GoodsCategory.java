package com.example.shoppingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GoodsCategory {
    EAT("식품")
    ;

    private final String name;
}
