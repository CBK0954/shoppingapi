package com.example.shoppingapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
