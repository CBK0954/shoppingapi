package com.example.shoppingapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OrderRequest {
    @ApiModelProperty(notes = "수신 주소")
    @NotNull
    @Length(min = 5, max = 100)
    private String receivedAddress;

    @ApiModelProperty(notes = "수신자")
    @NotNull
    @Length(min = 2, max = 20)
    private String receivedName;

    @ApiModelProperty(notes = "수신자 연락처")
    @NotNull
    @Length(min = 13, max = 20)
    private String receivedPhone;
}