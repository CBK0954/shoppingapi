package com.example.shoppingapi.model;

import com.example.shoppingapi.entity.Goods;
import com.example.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GoodsItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "카테고리")
    @Enumerated(EnumType.STRING)
    private String goodsCategory;

    @ApiModelProperty(notes = "섬네일 이미지 주소 (CDN")
    private String thumbImageUrl;

    @ApiModelProperty(notes = "상품명")
    private String goodsName;

    @ApiModelProperty(notes = "정가")
    private Double originPrice;

    @ApiModelProperty(notes = "할인가")
    private Double salePrice;

    @ApiModelProperty(notes = "최종판매가")
    private Double resultPrice;

    private GoodsItem(Builder builder) {
        this.id = builder.id;
        this.goodsCategory = builder.goodsCategory;
        this.thumbImageUrl = builder.thumbImageUrl;
        this.goodsName = builder.goodsName;
        this.originPrice = builder.originPrice;
        this.salePrice = builder.salePrice;
        this.resultPrice = builder.resultPrice;
    }

    public static class Builder implements CommonModelBuilder<GoodsItem> {
        private final Long id;
        private final String goodsCategory;
        private final String thumbImageUrl;
        private final String goodsName;
        private final Double originPrice;
        private final Double salePrice;
        private final Double resultPrice;

        public Builder(Goods goods) {
            this.id = goods.getId();
            this.goodsCategory = goods.getGoodsCategory().getName();
            this.thumbImageUrl = goods.getThumbImageUrl();
            this.goodsName = goods.getGoodsName();
            this.originPrice = goods.getOriginPrice();
            this.salePrice = goods.getSalePrice();
            this.resultPrice = goods.getOriginPrice() - goods.getSalePrice();
        }

        @Override
        public GoodsItem build() {
            return new GoodsItem(this);
        }
    }
}
