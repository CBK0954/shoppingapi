package com.example.shoppingapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartCreateRequest {
    private Long memberId;
    private Long goodsId;
}