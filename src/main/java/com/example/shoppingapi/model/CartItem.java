package com.example.shoppingapi.model;

import com.example.shoppingapi.entity.Cart;
import com.example.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CartItem {
    @ApiModelProperty(notes = "장바구니 시퀀스")
    private Long cartId;

    @ApiModelProperty(notes = "카테고리")
    private String goodsCategory;

    @ApiModelProperty(notes = "상품 섬네일 이미지")
    private String thumbImageUrl;

    @ApiModelProperty(notes = "상품명")
    private String goodsName;

    @ApiModelProperty(notes = "정가")
    private Double originPrice;

    @ApiModelProperty(notes = "할인금액")
    private Double salePrice;

    private CartItem(Builder builder) {
        this.cartId = builder.cartId;
        this.goodsCategory = builder.goodsCategory;
        this.thumbImageUrl = builder.thumbImageUrl;
        this.goodsName = builder.goodsName;
        this.originPrice = builder.originPrice;
        this.salePrice = builder.salePrice;
    }

    public static class Builder implements CommonModelBuilder<CartItem> {
        private final Long cartId;
        private final String goodsCategory;
        private final String thumbImageUrl;
        private final String goodsName;
        private final Double originPrice;
        private final Double salePrice;

        public Builder(Cart cart) {
            this.cartId = cart.getId();
            this.goodsCategory = cart.getGoods().getGoodsCategory().getName();
            this.thumbImageUrl = cart.getGoods().getThumbImageUrl();
            this.goodsName = cart.getGoods().getGoodsName();
            this.originPrice = cart.getGoods().getOriginPrice();
            this.salePrice = cart.getGoods().getSalePrice();
        }

        @Override
        public CartItem build() {
            return new CartItem(this);
        }
    }
}