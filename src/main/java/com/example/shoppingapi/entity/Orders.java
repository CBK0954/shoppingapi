package com.example.shoppingapi.entity;

import com.example.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Orders {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "회원")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ApiModelProperty(notes = "수신 주소")
    @Column(nullable = false, length = 100)
    private String receivedAddress;

    @ApiModelProperty(notes = "수신자")
    @Column(nullable = false, length = 20)
    private String receivedName;

    @ApiModelProperty(notes = "수신자 연락처")
    @Column(nullable = false, length = 20)
    private String receivedPhone;

    private Orders(Builder builder) {
        this.member = builder.member;
        this.receivedAddress = builder.receivedAddress;
        this.receivedName = builder.receivedName;
        this.receivedPhone = builder.receivedPhone;
    }

    public static class Builder implements CommonModelBuilder<Orders> {
        private final Member member;
        private final String receivedAddress;
        private final String receivedName;
        private final String receivedPhone;

        public Builder(Member member, String receivedAddress, String receivedName, String receivedPhone) {
            this.member = member;
            this.receivedAddress = receivedAddress;
            this.receivedName = receivedName;
            this.receivedPhone = receivedPhone;
        }

        @Override
        public Orders build() {
            return new Orders(this);
        }
    }
}
