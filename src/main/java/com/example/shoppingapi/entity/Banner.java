package com.example.shoppingapi.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Banner {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "배너 이미지 주소 (CDN)")
    @Column(nullable = false, length = 200)
    private String imageUrl;

    @ApiModelProperty(notes = "사용여부")
    @Column(nullable = false)
    private Boolean isUse;
}
