package com.example.shoppingapi.entity;

import com.example.shoppingapi.enums.GoodsCategory;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Goods {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "카테고리")
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private GoodsCategory goodsCategory;

    @ApiModelProperty(notes = "섬네일 이미지 주소 (CDN")
    @Column(nullable = false, length = 200)
    private String thumbImageUrl;

    @ApiModelProperty(notes = "상품명")
    @Column(nullable = false, length = 100)
    private String goodsName;

    @ApiModelProperty(notes = "정가")
    @Column(nullable = false)
    private Double originPrice;

    @ApiModelProperty(notes = "할인가")
    @Column(nullable = false)
    private Double salePrice;
}
