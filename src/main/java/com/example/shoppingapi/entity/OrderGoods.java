package com.example.shoppingapi.entity;

import com.example.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderGoods {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "주문")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private Orders orders;

    @ApiModelProperty(notes = "상품")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId", nullable = false)
    private Goods goods;

    private OrderGoods(Builder builder) {
        this.orders = builder.orders;
        this.goods = builder.goods;
    }

    public static class Builder implements CommonModelBuilder<OrderGoods> {
        private final Orders orders;
        private final Goods goods;

        public Builder(Orders orders, Goods goods) {
            this.orders = orders;
            this.goods = goods;
        }

        @Override
        public OrderGoods build() {
            return new OrderGoods(this);
        }
    }
}
