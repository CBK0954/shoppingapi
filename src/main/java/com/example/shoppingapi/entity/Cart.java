package com.example.shoppingapi.entity;

import com.example.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Cart {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "회원시퀀스")
    @Column(nullable = false)
    private Long memberId;

    @ApiModelProperty(notes = "상품")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId", nullable = false)
    private Goods goods;

    private Cart(Builder builder) {
        this.memberId = builder.memberId;
        this.goods = builder.goods;
    }

    public static class Builder implements CommonModelBuilder<Cart> {
        private final Long memberId;
        private final Goods goods;

        public Builder(Long memberId, Goods goods) {
            this.memberId = memberId;
            this.goods = goods;
        }

        @Override
        public Cart build() {
            return new Cart(this);
        }
    }
}