package com.example.shoppingapi.service;

import com.example.shoppingapi.entity.Cart;
import com.example.shoppingapi.entity.Goods;
import com.example.shoppingapi.model.CartItem;
import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.repository.CartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CartService {
    private final CartRepository cartRepository;

    public void setCart(long memberId, Goods goods) {
        Cart cart = new Cart.Builder(memberId, goods).build();
        cartRepository.save(cart);
    }

    public ListResult<CartItem> getCartsByMy(long memberId) {
        List<Cart> carts = cartRepository.findAllByMemberIdOrderByIdDesc(memberId);
        List<CartItem> result = new LinkedList<>();

        carts.forEach(item -> result.add(new CartItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void delCart(long cartId) {
        cartRepository.deleteById(cartId);
    }
}