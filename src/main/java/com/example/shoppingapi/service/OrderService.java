package com.example.shoppingapi.service;

import com.example.shoppingapi.entity.Cart;
import com.example.shoppingapi.entity.Member;
import com.example.shoppingapi.entity.Orders;
import com.example.shoppingapi.entity.OrderGoods;
import com.example.shoppingapi.model.OrderRequest;
import com.example.shoppingapi.repository.CartRepository;
import com.example.shoppingapi.repository.OrderGoodsRepository;
import com.example.shoppingapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final CartRepository cartRepository;
    private final OrderRepository orderRepository;
    private final OrderGoodsRepository orderGoodsRepository;

    public void setOrder(Member member, OrderRequest orderRequest) {
        Orders orders = new Orders.Builder(member, orderRequest.getReceivedAddress(), orderRequest.getReceivedName(), orderRequest.getReceivedPhone()).build();
        orderRepository.save(orders);

        List<Cart> carts = cartRepository.findAllByMemberIdOrderByIdDesc(member.getId());
        carts.forEach(item -> {
            OrderGoods orderGoods = new OrderGoods.Builder(orders, item.getGoods()).build();
            orderGoodsRepository.save(orderGoods);
            cartRepository.deleteById(item.getId());
        });
    }
}