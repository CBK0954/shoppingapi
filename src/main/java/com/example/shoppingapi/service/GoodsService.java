package com.example.shoppingapi.service;

import com.example.shoppingapi.entity.Goods;
import com.example.shoppingapi.enums.GoodsCategory;
import com.example.shoppingapi.exception.CMissingDataException;
import com.example.shoppingapi.model.GoodsItem;
import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public ListResult<GoodsItem> getGoodsByCategory(GoodsCategory goodsCategory) {
        List<Goods> goods = goodsRepository.findAllByGoodsCategoryOrderByGoodsNameAsc(goodsCategory);

        List<GoodsItem> result = new LinkedList<>();

        goods.forEach(e -> result.add(new GoodsItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

    public Goods getData(long id) {
        return goodsRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}