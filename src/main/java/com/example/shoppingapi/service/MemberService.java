package com.example.shoppingapi.service;

import com.example.shoppingapi.entity.Member;
import com.example.shoppingapi.exception.CMissingDataException;
import com.example.shoppingapi.model.LoginRequest;
import com.example.shoppingapi.model.LoginResponse;
import com.example.shoppingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public LoginResponse doLogin(LoginRequest loginRequest) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new);

        if (!member.getPassword().equals(loginRequest.getPassword())) throw new CMissingDataException();

        return new LoginResponse.Builder(member).build();
    }

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

}
