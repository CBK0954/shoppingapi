package com.example.shoppingapi.service;

import com.example.shoppingapi.entity.Banner;
import com.example.shoppingapi.model.ListResult;
import com.example.shoppingapi.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;

    public ListResult<String> getUseBanners() {
        List<Banner> banners = bannerRepository.findAllByIsUseOrderByIdAsc(true);

        List<String> result = new LinkedList<>();

        for (Banner banner : banners) result.add(banner.getImageUrl());

        return ListConvertService.settingResult(result);
    }
}
