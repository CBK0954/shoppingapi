package com.example.shoppingapi.repository;

import com.example.shoppingapi.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {
    List<Cart> findAllByMemberIdOrderByIdDesc(long memberId);
}