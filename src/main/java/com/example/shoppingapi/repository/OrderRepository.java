package com.example.shoppingapi.repository;

import com.example.shoppingapi.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
}