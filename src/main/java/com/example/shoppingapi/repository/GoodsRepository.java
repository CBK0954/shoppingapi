package com.example.shoppingapi.repository;

import com.example.shoppingapi.entity.Goods;
import com.example.shoppingapi.enums.GoodsCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
    List<Goods> findAllByGoodsCategoryOrderByGoodsNameAsc(GoodsCategory goodsCategory);
}
